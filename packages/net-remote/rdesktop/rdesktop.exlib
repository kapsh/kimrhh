# Copyright 2008 Kim Højgaard-Hansen <kimrhh@exherbo.org>
# Copyright 2013 Aleksandar Petrinic <petrinic@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require github [ release=v${PV} suffix=tar.gz ] \
    autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]

export_exlib_phases src_prepare src_configure

SUMMARY="Remote Desktop Protocol (RDP)"
DESCRIPTION="
rdesktop is an open source UNIX client for connecting to Windows Remote Desktop Services, capable
of natively speaking Remote Desktop Protocol (RDP) in order to present the user's Windows desktop.
rdesktop is known to work with Windows server versions ranging from NT 4 terminal server to Windows
Server 2012 R2.
"
HOMEPAGE+=" https://www.${PN}.org"

LICENCES="GPL-3"
SLOT="0"
MYOPTIONS="
    alsa
    ao
    libsamplerate [[ description = [ Use libsamplerate for higher quality sound
                                     resample. It's mandatory in case you want
                                     to play sound from a source with higher
                                     sampling rate than your card actually
                                     supports ] ]]
    oss

    ( alsa ao oss ) [[ number-selected = at-most-one ]]
    libsamplerate? ( ( alsa ao oss ) [[ number-selected = exactly-one ]] )
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

#FIXME: add pcsc-lite support and test it
DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        x11-libs/libX11
        x11-libs/libXrandr
        alsa? ( sys-sound/alsa-lib )
        ao? ( media-libs/libao )
        libsamplerate? ( media-libs/libsamplerate )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
"

rdesktop_src_prepare() {
    # Do not prestrip binaries
    edo sed -i -e '/$(STRIP)/d' Makefile.in

    autotools_src_prepare
}

rdesktop_src_configure() {
    local myconf="--with-sound=";
    myconf+=$(optionv alsa);
    myconf+=$(option ao libao);
    myconf+=$(option oss oss);

    econf \
        $(option_with libsamplerate) \
        ${myconf} \
        --with-ipv6 \
        --with-openssl="/usr/$(exhost --target)" \
        --disable-credssp \
        --disable-smartcard
}

